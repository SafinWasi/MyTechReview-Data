from flask import Flask
import flask_sqlalchemy
import sqlalchemy
from dotenv import load_dotenv
import os
import json

load_dotenv()

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = os.getenv("AWS_DEV_DB")
# print(os.getenv("AWS_DEV_DB"))
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = flask_sqlalchemy.SQLAlchemy(app)


class Product(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String())
    brand = db.Column(db.String())
    brand_id = db.Column(db.String())
    price = db.Column(db.Integer)
    memory = db.Column(db.String())
    ram = db.Column(db.String())
    os = db.Column(db.String())
    battery = db.Column(db.String())
    screen = db.Column(db.String())
    url = db.Column(db.String())
    image = db.Column(db.String())

    def __init__(self, name="N/A", brand="N/A", brand_id=-1, price=-1, memory="N/A", ram="N/A", os="N/A", battery="N/A", screen="N/A", url="N/A", image="N/A"):
      self.name = name
      self.brand = brand
      self.brand_id = brand_id
      self.price = price
      self.memory = memory
      self.ram = ram
      self.os = os
      self.battery = battery 
      self.screen = screen
      self.url = url
      self.image = image

with app.app_context():
    db.create_all()

# get parsed jsons
with open('parsed_data/products.json', 'r') as f:
  data = json.loads(f.read())
product_list = []
test = 0
for item in data:
  # if(test > 100):
  #   break
  new_product = Product(name=item["name"], brand=item["brand"], brand_id=item["brand_id"], price=item["price"], memory=item["memory"], ram=item["ram"], os=item["os"], battery=item["battery"], screen=item["screen"], url=item["url"], image=item["image"])
  product_list.append(new_product)
  # test+=1

db.session.add_all(product_list)
db.session.commit()
