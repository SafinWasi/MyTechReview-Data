import fetch from 'node-fetch';
import fs from 'fs';
import jsdom from 'jsdom';
import parsed_bb from './parsed_data/parsed_bb.json';

const { JSDOM } = jsdom;

const getData = async (url) => {
    try {
        const res = await fetch(url, {
            "credentials": "include",
            "headers": {
                "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0",
                "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
                "Accept-Language": "en-GB,en;q=0.5",
                "Upgrade-Insecure-Requests": "1",
                'Cookie': 'oid=673146967; vt=57bb5862-a869-11eb-a024-02dcac2b24d1; partner=198%26Narrativ%262021-04-28+16%3a33%3a16.000; _abck=271C9D70C6221609867E401332A3ADA7~0~YAAQV5ERYFhvw1l8AQAAyBQLnwZm1KvE7+OVvOO1MvAR5MJkfn86XBzvqQ9Rp7kz+5hshL+FnucV48NSjPZF/BimMtvophdUF+YZwxzDIeyetKfqm7cfR4+vYr0E3Fp3MGG9RyWVoP4DLfNWgpDR2glNz5Muf1rE4+Z3flw4/Q6/llQQWxpPIp/qzT9kpE2+p5ax0ktQaDKXZ3j6l+3JtvfKhNkswuwvsEGpa3I30B8Sx3Km0wxuDvzmVqSKauwAVXLui7rXYs/WgR2IAjQ2/+jz2aAs5H0Y3+4zLju4mw+xu5CqTPRUGPTgUmeVSo6MaK/btehjU0Gg7/ILcG0XX+VnDWpQsh/MH8n/8e/qvzHujEq9pPjfHezZCi7J+dbkPpN5PktZjopP9b0tIhqjK72HX0JAxkq6Zrre45jpTS639GXqmYIfN9I2gto=~-1~-1~-1; AMCV_F6301253512D2BDB0A490D45%40AdobeOrg=1585540135%7CMCMID%7C14622014078733041144170649929104532135%7CMCAID%7CNONE%7CMCOPTOUT-1619652816s%7CNONE%7CMCAAMLH-1620250416%7C9%7CMCAAMB-1620250416%7Cj8Odv6LonN4r3an7LhD3WZrU1bUpAkFkkiY1ncBR96t2PTI%7CMCCIDH%7C403498598%7CvVersion%7C4.4.0; s_ecid=MCMID%7C14622014078733041144170649929104532135; _cs_c=1; _cs_id=b94d587d-cccd-a13c-e9ad-3d177c7ac423.1619645618.1.1619645618.1619645618.1614963257.1653809618423.Lax.0; analyticsStoreId=204; analyticsToken=57bb5862-a869-11eb-a024-02dcac2b24d1; sc-location-v2=%7B%22meta%22%3A%7B%22CreatedAt%22%3A%222021-04-28T21%3A34%3A20.846Z%22%2C%22ModifiedAt%22%3A%222021-04-28T21%3A34%3A21.842Z%22%2C%22ExpiresAt%22%3A%222022-04-28T21%3A34%3A21.842Z%22%7D%2C%22value%22%3A%22%7B%5C%22physical%5C%22%3A%7B%5C%22zipCode%5C%22%3A%5C%2273301%5C%22%2C%5C%22source%5C%22%3A%5C%22A%5C%22%2C%5C%22captureTime%5C%22%3A%5C%222021-04-28T21%3A34%3A20.843Z%5C%22%7D%2C%5C%22destination%5C%22%3A%7B%5C%22zipCode%5C%22%3A%5C%2273301%5C%22%7D%2C%5C%22store%5C%22%3A%7B%5C%22storeId%5C%22%3A204%2C%5C%22zipCode%5C%22%3A%5C%2278735%5C%22%2C%5C%22storeHydratedCaptureTime%5C%22%3A%5C%222021-04-28T21%3A34%3A21.841Z%5C%22%7D%7D%22%7D; locDestZip=73301; locStoreId=204; pst2=204|N; CTT=c3dcce6632fde512f0536d87b7d6156a; basketTimestamp=1634521478430; CTE11=T; bm_sz=A249BC65A40C0FD50F52775F4154D304~YAAQh5ERYNlSRld8AQAAQjo2nw19mr56gi1izbIdaWlF+i4k8YDQjVuKuVvbLFIfuA/CBboShvNfT3cE+aMJ9BDta/DQCQkuMvDiWCho3O2vRxOOGJh3dexDtI5/7LCY5y2I9jCCuPYchcmkcURzmgfAXshxKLXvnRl6OGSCzfVUVrO2TOWdkMtqVyDd9u6GvOHP+zj+7QQ5qm6JrD5pLxCkdr9x0ASnBfQivmir8AnlVJYM8gnf3m2q9AtDVgVxH6ldxpYGp0LJLmcQrvLc04qStIPykc98pD50q913+bnJPnvCRflwKuNKo0D5uNfh+bN+TniJqAIbQEbBkd/biHLe8jHuzSk3OvOP3vtYJSW8XzsifEFWfPQcOuRFNNZCfmcD5Nk7FJ8HyInT9e1OiushOAkjpVdTy3u/n2+zoveCi4b3bfo=~4277554~3747888; SID=6bd3a01f-a0c6-40c3-b2ec-00c3d14ce35a; physical_dma=635; customerZipCode=73301|N; bby_rdp=l; dtCookie=v_4_srv_10_sn_EMN0CVDNB8P7H38PC3F417H0ATQV8QLO_app-3Aea7c4b59f27d43eb_1_app-3A1b02c17e3de73d2a_1_ol_0_perc_100000_mul_1; rxVisitor=1634755640607P1GRN4II5UOGCISB3HD0QIRENLDGCDFE; dtPC=10$357060433_140h-vFKUNAAFHBPRUWAFVKLCKCPCADJRFRKDE-0e0; rxvt=1634760661142|1634758861142; dtSa=-; COM_TEST_FIX=2021-10-20T18%3A47%3A20.644Z; dtLatC=4; bby_cbc_lb=p-browse-w; ltc=%20; bby_prc_lb=p-prc-w'
            },
            "method": "GET",
            "mode": "cors"
        });

        return res.text();
    } catch (e) {
        console.log(e)
    }
}

const getProductReviewsUrl = (product) => {
    const name = "ASUS - M241DA 23.8'' Touch-Screen All-In-One - AMD R5-3500U - 8GB Memory - 256GB Solid State Drive - Black - Black";
    const urlProductName = name.replace(/[ .,]/g, '-').replace(/[()']/g, '').replace(/-+/g, '-').toLowerCase();
    const sku = product.url.substring(32, 39);
    return `https://www.bestbuy.com/site/reviews/${urlProductName}/${sku}`;
}

const pageReviews = (productId, document) => {
    return [...document.querySelectorAll('li.review-item')].map(li => {
        const row = li.firstChild;
    
        // Find Username and features (value, quality and Ease of use)
        const header = row.firstChild.firstChild;
    
        const username = header.firstChild.firstChild.firstChild.firstChild.firstChild.firstChild.data;      
    
        const features = [...header.lastChild.firstChild.lastChild.children];
    
        const valueElem = features.find(child => child.className.split(" ").includes("Value-rating"));
        const easeOfUseElem = features.find(child => child.className.split(" ").includes("EaseofUse-rating"));
        const qualityElem = features.find(child => child.className.split(" ").includes("Quality-rating"));

        let numValue = -1;
        let numEaseOfUse = -1;
        let numQuality = -1;
    
        if (valueElem?.lastChild?.firstChild?.innerText)
            numValue = parseInt(valueElem.lastChild.firstChild.innerText.substring(0, 1));
        
        if (easeOfUseElem?.lastChild?.firstChild?.innerText)
            numEaseOfUse = parseInt(easeOfUseElem.lastChild.firstChild.innerText.substring(0, 1));
    
        if (qualityElem?.lastChild?.firstChild?.innerText)
            numQuality = parseInt(qualityElem.lastChild.firstChild.innerText.substring(0, 1));
    
        // Find title, numStars, description, purchase date, helpful, unhelpful
        const reviewSection = row.lastChild;
        const reviewSectionChildren = [...reviewSection.children];
    
        // Find title and numStars
        const reviewHeading = reviewSectionChildren.find(child => child.className.split(" ").includes("review-heading"));
    
        const starText = reviewHeading.firstChild.firstChild.firstChild.firstChild.data;
        const numStars = parseInt(starText.substring(6, 7));
    
        const title = reviewHeading.lastChild.firstChild.data;
    
        // Find submission time
        const reviewContext = reviewSectionChildren.find(child => child.className.split(" ").includes("review-context"));
        const submissionElem = [...reviewContext.children].find(child => child.className.split(" ").includes("disclaimer")).children[0];
        const submissionTime = new Date(submissionElem.title);
    
        // Find description
        const reviewBody = reviewSectionChildren.find(child => child.className.split(" ").includes("ugc-review-body"));
        const description = reviewBody.firstChild.firstChild.data;
    
        // Find numHelpful and numUnhelpful
        const reviewFeedback = reviewSectionChildren.find(child => child.className.split(" ").includes("ugc-feedback-container")).firstChild.firstChild.firstChild;
    
        const helpfulText = reviewFeedback.children[0].firstChild.data;
        const unhelpfulText = reviewFeedback.children[1].firstChild.data;
    
        const numHelpful = parseInt(helpfulText.substring(9, helpfulText.length - 1));
        const numUnhelpful = parseInt(unhelpfulText.substring(11, unhelpfulText.length - 1));
    
        return {
            username,
            title,
            description,
            numStars,
            numHelpful,
            numUnhelpful,
            numValue,
            numEaseOfUse,
            numQuality,
            productId,
            submissionTime
        }
    }).filter(review => Object.keys(review).length !== 0);
}

const productReview = async (product) => {
    try {
        const url = product.url;

        const data = await getData(url);
        const { document } = (new JSDOM(data)).window;
    
        return pageReviews(product.id, document);
    } catch (e) {
    }
}

const scrape = async () => {
    // [ { id: 0, url: 'https://www.bestbuy.com/site/reviews/...'}]
    const products = parsed_bb.map(product => ({ id: product.id, url: getProductReviewsUrl(product) }));

    // Get reviews
    let reviews = (await Promise.all(products.map(product => productReview(product)))).flat();
    
    // Assign Id's
    reviews = reviews.map((review, id) => ({ ...review, id}));

    fs.writeFile('./parsed_data/reviews.json', JSON.stringify(reviews), (err) => console.log(err));
}

scrape();