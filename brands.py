from flask import Flask
import flask_sqlalchemy
import sqlalchemy
from dotenv import load_dotenv
import os
import json

load_dotenv()

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = os.getenv("AWS_DEV_DB")
# print(os.getenv("AWS_DEV_DB"))
# app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = flask_sqlalchemy.SQLAlchemy(app)

class Brand(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text)
    numProducts = db.Column(db.Integer)
    founded = db.Column(db.Integer)
    revenue = db.Column(db.Float)
    num_employees = db.Column(db.Text)
    url = db.Column(db.Text)
    description = db.Column(db.Text)
    wikiUrl = db.Column(db.Text)
    img = db.Column(db.Text)
    country = db.Column(db.Text)

    # def __init__(self, id=-1, product_id=-1, name="N/A", product_num=-1, founded=-1, revenue=-1, num_employees=-1, url="N/A", description="N/A", wiki_url="N/A"):
    #     self.id = id
    #     self.product_id = product_id
    #     self.name = name
    #     self.product_num = product_num
    #     self.founded = founded
    #     self.revenue = revenue
    #     self.num_employees = num_employees
    #     self.url = url
    #     self.description = description
    #     self.wiki_url = wiki_url
        
with app.app_context():
    db.create_all()

# get parsed jsons
with open('parsed_data/brands.json', 'r') as f:
  data = json.loads(f.read())
# brand_list = []
for item in data:
  new_brand = Brand(id=item["id"], name=item["name"], numProducts=item["numProducts"], founded=item["founded"], revenue=item["revenue"], num_employees=item["num_employees"], url=item["url"], description=item["description"], wikiUrl=item["wikiUrl"], img=item["img"], country=item["country"])
  # brand_list.append(new_brand)
  db.session.add(new_brand)

# db.session.add_all(brand_list)
db.session.commit()