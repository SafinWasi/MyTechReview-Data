import json
import requests

with open("config.json", "r") as f:
    input = f.read()

config = json.loads(input)
NB_KEY = config["noteb"]["key"]
NB_URL = config["noteb"]["url"]
MB_URL = config["mobilespecs"]["url"]

def noteb_request(method, param):
    data = {"apikey":str(NB_KEY), "method":str(method), param[0]:param[1]}
    r = requests.post(NB_URL, data=data)
    print(r.json())
    return r.json()

def mb_request(method):
    r = requests.get(MB_URL + method)
    print(r.json())
    return r.json()


noteb_request("list_models", ["param[0]", ""])